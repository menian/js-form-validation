
/* this code is from http://menian-lee.eu/ 
You can find there the full tutorial by 
searching Javascript form validation */

var valid = 0;


/* This is the main function, where we call
other function. In that function we also place the code for submitting the form */

function ValidateForm()
	{
	
	/* you can use few words for colors 
	and hex code colors: #3ffff3 for example */
	
	var errorColor = "red";
	var correctColor = "green";


	/* with this code we get the values from the 
	input field and turn them into javascript variables */
	
	var vFirstName = document.getElementById("firstName");
	var vEmail = document.getElementById("email").value;
	var vTopic = document.getElementById("topic");
	var vMessage = document.getElementById("message");


	/* here we call functions in a function and also pass values
	for max min length... */
	
	ValidateString(vFirstName,2,20,correctColor,errorColor);
	ValidateString(vTopic,4,20,correctColor,errorColor);
	ValidateString(vMessage,4,399,correctColor,errorColor);
	ValidateEmail(vEmail,correctColor,errorColor);

	/* in each of the other 4 functions we add 1 to valid
		here we submit the form if valid get bigger than 3 */
	
		if (valid >3)
		{
			 document.getElementById("ContactForm").submit();
		}

	}

	/* this function is called 3 times, but with different values.
		it's used to validate the 3 string we have in 
		the form: for first name, title, message*/
	
function ValidateString(obj, minLength, maxLength, correctColor, errorColor)
	{
		if(obj.value.length < minLength || obj.value.length > maxLength)
		{
			obj.style.borderColor = errorColor;
			valid = 0;

		}
		
		else
		{
			obj.style.borderColor = correctColor;
			
			// ++ should be read as +1 to the variable valid
			valid++;
		}
	}

	/* This code is from http://www.w3schools.com/js/js_form_validation.asp
		of course with small modifications to make it work in our case here */
	
function ValidateEmail(email, correctColor, errorColor)
	{
		var atpos=email.indexOf("@");
		var dotpos=email.lastIndexOf(".");
		
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
		  {
			valid= 0;
			document.getElementById("email").style.borderColor = errorColor;
			
		  }
		  else
		  {
			document.getElementById("email").style.borderColor = correctColor;

			valid++;
		  }
}